<?php
//Begin Really Simple SSL session cookie settings
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);
//END Really Simple SSL

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'u1312163_marts' );

/** MySQL database username */
define( 'DB_USER', 'u1312163_marts' );

/** MySQL database password */
define( 'DB_PASSWORD', 'P@ssword123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$@ucn>+*-D;bv?Y.lHF)u_O$G3kt)%ct}pJufuEN-iC!mTs=q(E[RmfMEr4p(yZJ' );
define( 'SECURE_AUTH_KEY',  'sFqD4{uX8IAp&6c^iDsfRqfCN!^)i(d_N8fY>Wwuhk~Fyt@7V3r@#Q b]o+M`4h&' );
define( 'LOGGED_IN_KEY',    '`GS|@Xq>*[7S@ytr<)6D*m @[i{r$B|kNTXci_;30WUERV4pD/]d_$gM^+pN>`^`' );
define( 'NONCE_KEY',        'iI*h8JdhiT8):!|bIH_E=0_&/c`;[r_VR-_Lw;]|gW:P&q}y-Ne.c!4H$?3#)9x1' );
define( 'AUTH_SALT',        'E3E4Y r-gmlz4:04H|.8rChWc- |!V;49/N@(2(yR5 -WT|j^(rU^bQBsXBjlrUf' );
define( 'SECURE_AUTH_SALT', '*6nqxp9VS;Zg[@xPJ6xvSE24!BHwqkYj DP8>wD.m_%3TW(nGQXpIu_i)8UO_Lz`' );
define( 'LOGGED_IN_SALT',   'K&-(^nFgmCmUXUnVc7[Fd^_9_abAZh(SRWnGeUOjmu&RRAq{S1cGb9{<iApMz2KN' );
define( 'NONCE_SALT',       'sgceOwN(~,!2C @SQ87j6_69?-n8Z8*,)D#wcM.rfIW !-]58IIm<a,fK4d6fUMf' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
